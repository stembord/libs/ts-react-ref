import * as Enzyme from "enzyme";
import * as EnzymeAdapter from "enzyme-adapter-react-16";
import { JSDOM } from "jsdom";
import * as React from "react";
import * as tape from "tape";
import { getStembordUrl, StembordRef } from "../lib";

const dom = new JSDOM("<!doctype html><html><body></body></html>");

(global as any).document = dom.window.document;
(global as any).window = dom.window;

Enzyme.configure({ adapter: new EnzymeAdapter() });

tape("StembordRef", (assert: tape.Test) => {
  const ref = Enzyme.shallow(
    <StembordRef name="stembord/libs/ref">Stembord Ref</StembordRef>
  );
  assert.equals(
    (ref.instance() as StembordRef).getHref(),
    `${getStembordUrl()}/stembord/libs/ref`
  );
  assert.end();
});
