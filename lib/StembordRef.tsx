import * as React from "react";

if (!(global as any).__STEMBORD_URL__) {
  (global as any).__STEMBORD_URL__ = "https://stembord.com";
}

export const setStembordUrl = (url: string) => {
  (global as any).__STEMBORD_URL__ = url;
};

export const getStembordUrl = () => (global as any).__STEMBORD_URL__;

export interface IStembordRefProps extends React.AnchorHTMLAttributes<any> {
  name: string;
  children: React.ReactNode;
}

export class StembordRef extends React.Component<IStembordRefProps> {
  getHref() {
    return `${getStembordUrl()}/${this.props.name}`;
  }
  render() {
    const { name, children, ...props } = this.props;

    return (
      <a {...props} href={this.getHref()}>
        {children}
      </a>
    );
  }
}
